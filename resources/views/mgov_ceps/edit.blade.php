@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h2 class="d-flex">Mgov Ceps - Editar o id {{ $result->id }}</h2>
            <hr>
        </div><!-- ./col-12 -->
        <div class="col-12">
            <form method="post" action="{{ route('mgov-ceps.update', $result->id) }}">
                {!! csrf_field() !!}
                <input name="_method" type="hidden" value="PUT">
                <div class="form-group">
                    <label for="mgov_id">ID da Mgov</label>
                    <input type="text" name="mgov_id" id="mgov_id" class="form-control" placeholder="Exemplo: 05736" value="{{ $result->mgov_id}}">
                </div>
                <div class="form-group">
                    <label for="cep">CEP</label>
                    <input type="text" name="cep" id="cep" class="form-control" placeholder="Exemplo: 13212-412" value="{{ $result->cep}}">
                </div>
                <div class="form-group">
                    <label for="pais">País</label>
                    <input type="text" name="pais" id="pais" class="form-control" placeholder="Exemplo: Brasilzão" value="{{ $result->pais}}">
                </div>
                <button class="btn btn-block btn-success" type="submit">Salvar</button>
            </form> <br><br>
            <a href="{{ route('mgov-ceps.index') }}">Voltar para a listagem</a> <br><br>
        </div><!-- ./col-12 -->
    </div><!-- ./row -->
</div><!-- ./container -->
@endsection
