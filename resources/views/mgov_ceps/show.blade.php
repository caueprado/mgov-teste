@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h2 class="d-flex">Mgov Ceps</h2>
            <hr>
        </div><!-- ./col-12 -->
        <div class="col-12">
            ID: {{ $result->id }} <br />
            Mgov Id: {{ $result->mgov_id }} <br />
            CEP: {{ $result->formatCepAttribute($result->cep) }} <br />
            País: {{ $result->pais }} <br /><br /><br />
            <a href="{{ route('mgov-ceps.index') }}">Voltar para a listagem</a> <br><br>
            <a href="{{ route('mgov-ceps.edit', $result->id) }}">Editar o item</a>
        </div><!-- ./col-12 -->
    </div><!-- ./row -->
</div><!-- ./container -->
@endsection
