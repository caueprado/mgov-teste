@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h2 class="d-flex">Mgov Ceps</h2>
            <div class="d-flex">
                <div class="align-items-center">
                    <a class="btn btn-link" href="{{route('mgov-ceps.create')}}">Criar novo Mgov Cep - Id</a>
                </div>
                <div class="ml-auto">
                    <form enctype="multipart/form-data" method="post" action="{{ route('import-excel-mgov-template.store') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input type="file" name="file" id="file" class="form-control" aria-describedby="Selecione uma Planilha">
                            <button type="submit" class="btn btn-link">Enviar planilha</button>
                            <small id="helpId" class="text-muted">Selecione um arquivo .xls ou .xlsx</small>
                        </div>
                    </form>
                </div>

            </div>
            <hr>
        </div><!-- ./col-12 -->
        <div class="col-12">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Mgov ID</th>
                        <th>CEP</th>
                        <th>País</th>
                        <th>Excluir?</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($results as $result)
                    <tr>
                        <td><a class="btn btn-link" href="{{ route('mgov-ceps.show', $result->id) }}">{{ $result->id }}</a></td>
                        <td>{{ $result->mgov_id }}</td>
                        <td>{{ $result->formatCepAttribute($result->cep) }}</td>
                        <td>{{ $result->pais }}</td>
                        <td>
                            <form action="{{ route('mgov-ceps.destroy', $result->id) }}" class="form-horizontal" method="post" style="display: inline-block">
                                {!! csrf_field() !!}
                                <input type="hidden" name="_method" value="DELETE">
                                <button type="submit" class="btn btn-secondary">
                                    <span>
                                        <i class="fa fa-ban" aria-hidden="true"></i>
                                    </span>
                                </button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <footer>
                {{ $results->render('vendor.pagination.bootstrap-4') }}
            </footer>
        </div><!-- ./col-12 -->
    </div><!-- ./row -->
</div><!-- ./container -->
@endsection
