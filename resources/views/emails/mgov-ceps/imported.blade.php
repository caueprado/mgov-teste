@component('mail::message')
# Olá {{env('MAIL_PLAN_NAME', '')}}

O arquivo do excel foi importado com
@if($result->negativeResult->count() < 1)
 sucesso!
@elseif($result->positiveResult->count() > 0 && $result->negativeResult->count() > 0)
 sucesso, mas tivemos alguns problemas
@elseif($result->positiveResult->count() < 1)
 erros, logo abaixo você verá quais foram os problemas
@endif
. O total de itens que tinha na sua planilha do excel eram {{ $result->totalItens }} itens.

@if($result->negativeResult->count() > 0)
Houveram erros nas seguintes linhas do seu arquivo do excel:<br><br>
@foreach($result->negativeResult->all() as $erro)
Linha: {{ $erro['row'] }}. <br>
Motivo:
@if($erro['reason'])
{{ $erro['reason'] }}. <br><br>
@else
Sem motivo gerado, deve ser erro do sistema (opsss).<br><br>
@endif
@endforeach
<br><br>Uffa, terminou a lista. Agora volte no seu arquivo do excel e resolva os problemas. Não se preocupe com o que já foi importado, nosso sistema é inteligente o suficiente para não criar duas o mesmo registro que está na sua planilha, caso você realize uma alteração em um registro que já foi criado, o sistema irá atualizar esse registro automagicamente (rsrs).
@endif

Obrigado,<br>
@endcomponent
