@component('mail::message')
# Olá {{env('MAIL_PLAN_NAME', '')}},

A Tabela de Mgov Location foi atualizado a partir da tabela Mgov Ceps, os dados de latitude e longitude vieram direto do Google Maps (qualquer problema é com eles que você briga ok? rsrs).

@if($errors->count() > 0)
Olha, infelizmente houveram erros, mas não se preocupe, matenha a calma e procure ver mais detalhes sobre "esse" CEP, o id dele vai estar logo abaixo: <br><br>
@foreach ($errors->all() as $error)
ID: {{$error['item']}}<br>
Razão do Erro: {{$error['reason']}}<br>
@endforeach
<br><br>Peço desculpa pelo incomodo. Qualquer problema ou dúvida, procure o setor de TI mais próximo.
@endif

Obrigado,<br>
{{ config('app.name') }}
@endcomponent
