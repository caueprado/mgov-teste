@extends('layouts.app')
@section('content')
<main class="container all-height">
    <div class="row">
        <div class="col-12">
            <section class="d-flex justify-content-center flex-column align-items-center">
                <div class="card w-50">
                    <h4 class="card-header">Login</h4>
                    <div class="card-body">
                        <form action="{{ route('login') }}" method="POST" accept-charset="utf-8">
                            <div class="row">
                                {{ csrf_field() }}
                                <div class="col-12 mb-3">
                                    <label for="email">E-mail:</label>
                                    <input type="email" id="email" name="email" value="{{ old('email') }}" required autofocus class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}">
                                    @if ($errors->has('email'))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </div>
                                    @endif
                                </div><!-- ./ col-12 col-md-6 mb-3 email -->
                                <div class="col-12 mb-3">
                                    <label for="password">Password:</label>
                                    <input type="password" id="password" name="password" value="{{ old('password') }}" required class="form-control {{ ($errors->has('password') || $errors->has('email') )? ' is-invalid' : '' }}">
                                    @if ($errors->has('password'))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </div>
                                    @endif
                                </div><!-- ./ col-12 col-md-6 mb-3 password -->
                                <div class="col-12 mb-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" name="remember" class="custom-control-input" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <label class="custom-control-label" for="remember">Lembrar a senha?</label>
                                    </div>
                                </div><!-- ./ col-12 col-md-6 mb-3 remember-password -->
                                <div class="col-12 mb-3">
                                    <div class="d-flex flex-column justify-content-start">
                                        <div class="col-12 p-0">
                                            <button class="m-0 btn btn-block btn-success" type="submit">
                                                Login
                                            </button>
                                        </div>
                                        <div class="mt-2">
                                            <a class="p-0 btn btn-link" href="{{ route('password.request') }}">
                                                Esqueceu sua senha?
                                            </a>
                                        </div>
                                    </div>
                                </div><!-- ./ col-12 submit-->
                            </div><!-- ./row -->
                        </form>
                    </div><!-- ./card-body -->
                </div><!-- ./.card -->
            </section>
        </div><!-- ./col-12 -->
    </div><!-- ./row -->
</main><!-- ./container -->
@endsection
