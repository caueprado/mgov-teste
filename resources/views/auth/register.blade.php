@extends('layouts.app')

@section('content')
<main class="container all-height">
    <div class="row">
        <div class="col-12">
            <section class="d-flex justify-content-center flex-column align-items-center">
                <div class="card w-50">
                    <h4 class="card-header">Registro</h4>
                    <div class="card-body">
                        <form action="{{ route('register') }}" method="POST" accept-charset="utf-8">
                            <div class="row">
                                {{ csrf_field() }}

                                <div class="col-12 mb-3">
                                    <label for="name">Nome:</label>
                                    <input type="text" id="name" name="name" value="{{ old('name') }}" required autofocus class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}">
                                    @if ($errors->has('name'))
                                        <div class="invalid-feedback">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </div>
                                    @endif
                                </div><!-- ./ col-12 col-md-6 mb-3 name -->

                                <div class="col-12 mb-3">
                                    <label for="email">E-mail:</label>
                                    <input type="email" id="email" name="email" value="{{ old('email') }}" required autofocus class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}">
                                    @if ($errors->has('email'))
                                        <div class="invalid-feedback">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </div>
                                    @endif
                                </div><!-- ./ col-12 col-md-6 mb-3 email -->

                                <div class="col-12 mb-3">
                                    <label for="password">Senha:</label>
                                    <input type="password" id="password" name="password" value="{{ old('password') }}" required class="form-control {{ ($errors->has('password') || $errors->has('email') )? ' is-invalid' : '' }}">
                                    @if ($errors->has('password'))
                                        <div class="invalid-feedback">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </div>
                                    @endif
                                </div><!-- ./ col-12 col-md-6 mb-3 password -->

                                <div class="col-12 mb-3">
                                    <label for="password_confirmation">Confirme sua Senha:</label>
                                    <input type="password" id="password_confirmation" name="password_confirmation" required class="form-control">
                                </div><!-- ./ col-12 col-md-6 mb-3 password_confirmation -->

                                <div class="col-12 mb-3">
                                    <div class="d-flex flex-column justify-content-start">
                                        <div class="col-12 p-0">
                                            <button class="m-0 btn btn-block btn-dark" type="submit">
                                                Registrar
                                            </button>
                                        </div>
                                    </div>
                                </div><!-- ./ col-12 submit-->
                            </div><!-- ./row -->
                        </form>
                    </div><!-- ./card-body -->
                </div><!-- ./.card -->
            </section>
        </div><!-- ./col-12 -->
    </div><!-- ./row -->
</main><!-- ./container -->
@endsection
