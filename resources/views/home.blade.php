@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="jumbotron jumbotron-fluid">
                <div class="container">
                    <h1 class="display-4">Mgov Teste</h1>
                    <p class="lead">Seja bem vindo {{ Auth::user()->name }}. Esse é o meu teste para a Empresa <strong>Mgov</strong>, a ideia aqui é testar funcionalidades além do CRUD básico, como por exemplo, o uso de Eventos, Filas, Manipulação de Arquivos, etc.</p>

                    <p>Sinta-se à vontade para entrar em contato comigo nos seguintes canais:</p>
                    <p>
                        <i class="fa fa-whatsapp" aria-hidden="true"></i>
                        (11)98128-7535
                    </p>
                    <p>
                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                        <a target="_blank" href="https://www.linkedin.com/in/caueprado/">Cauê Prado</a>
                    </p>
                    <p>
                        <i class="fa fa-envelope" aria-hidden="true"></i>
                        <a target="_blank" href="mailto:caue.prado0@gmai.com">Me envie um e-mail.</a>
                    </p>
                    <p>
                        <i class="fa fa-github" aria-hidden="true"></i>
                        <a target="_blank" href="https://github.com/caueprado0">Vamos codar?</a>
                    </p>
                </div>
            </div>
        </div><!-- ./col-12 -->
    </div><!-- ./row -->
</div><!-- ./container -->
@endsection
