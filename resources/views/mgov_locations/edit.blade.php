@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h2 class="d-flex">Mgov Ceps - Editar o id {{ $result->id }}</h2>
            <hr>
        </div><!-- ./col-12 -->
        <div class="col-12">
            <form method="post" action="{{ route('mgov-locations.update', $result->id) }}">
                {!! csrf_field() !!}
                <input name="_method" type="hidden" value="PUT">
                <div class="form-group">
                    <label for="mgov_ceps_id">ID da Mgov Cep Id (se não sabe, vai lá na Mgov Cep consultar!)</label>
                    <input type="text" name="mgov_ceps_id" id="mgov_ceps_id" class="form-control" placeholder="Exemplo: 1 (como assim não sabe o id??? hahaha)" value="{{ $result->mgov_ceps_id}}">
                    <small id="helpId" class="text-muted">ATENÇÃO, não se preocupe em digitar uma latitude errada, pois, antes de salvar eu vou verificar se a latitude bate com o que tem no Google Maps (desculpe se confio mais nele do que em você, rsrs).</small>
                </div>
                <div class="form-group">
                    <label for="latitude">Latitude</label>
                    <input type="text" name="latitude" id="latitude" class="form-control" placeholder="Exemplo: -23.1734144" value="{{ $result->latitude}}">
                </div>
                <div class="form-group">
                    <label for="longitude">Longitude</label>
                    <input type="text" name="longitude" id="longitude" class="form-control" placeholder="Exemplo: -46.9619862" value="{{ $result->longitude}}">
                    <small id="helpId" class="text-muted">ATENÇÃO, não se preocupe em digitar uma longitude errada, pois, antes de salvar eu vou verificar se a latitude bate com o que tem no Google Maps (desculpe se confio mais nele do que em você, rsrs).</small>
                </div>
                <button class="btn btn-block btn-success" type="submit">Salvar</button>
            </form> <br><br>
            <a href="{{ route('mgov-locations.index') }}">Voltar para a listagem</a> <br><br>
        </div><!-- ./col-12 -->
    </div><!-- ./row -->
</div><!-- ./container -->
@endsection
