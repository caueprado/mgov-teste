@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h2 class="d-flex">Mgov Locations</h2>
            <hr>
        </div><!-- ./col-12 -->
        <div class="col-12">
            ID: {{ $result->id }} <br />
            CEP: {{ isset($result->mgovCep->cep)? $result->mgovCep->cep:'Ooops... Não consegui encontrar o cep.' }} <br />
            Latitude: {{ $result->latitude }} <br />
            Longitude: {{ $result->longitude }} <br /><br /><br />
            <a href="{{ route('mgov-locations.index') }}">Voltar para a listagem</a> <br><br>
            <a href="{{ route('mgov-locations.edit', $result->id) }}">Editar o item</a>
        </div><!-- ./col-12 -->
    </div><!-- ./row -->
</div><!-- ./container -->
@endsection
