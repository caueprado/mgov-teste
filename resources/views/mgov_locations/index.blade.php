@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h2 class="d-flex">Mgov Locations</h2>
            <div class="d-flex">
                <div class="align-items-center">
                    <a class="btn btn-link" href="{{route('mgov-locations.create')}}">Criar uma nova Geolocalização para determinado MgovId</a>
                </div>
                <div class="ml-auto">
                    <form method="post" action="{{ route('create-lat-lng-from-mgov-ceps.location') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <button type="submit" class="btn btn-link">Atualizar</button>
                            <small id="helpId" class="text-muted">Nesta opção você atualiza a localização de acordo com todos os ceps que estão na tabela mgov_ceps. Esse processo leva um tempo, por isso, no fim, você receberá um e-mail avisando o termino.</small>
                        </div>
                    </form>
                </div><!-- ml-auto -->
            </div><!-- d-flex -->
            <hr>
        </div><!-- ./col-12 -->
        <div class="col-12">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>CEP</th>
                        <th>Latitude</th>
                        <th>Longitude</th>
                        <th>Excluir?</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($results as $result)
                    <tr>
                        <td><a class="btn btn-link" href="{{ route('mgov-locations.show', $result->id) }}">{{ $result->id }}</a></td>
                        <td>{{ isset($result->mgovCep->cep)? $result->mgovCep->cep:'Ooops... Não consegui encontrar o cep.' }}</td>
                        <td>{{ $result->latitude }}</td>
                        <td>{{ $result->longitude }}</td>
                        <td>
                            <form action="{{ route('mgov-locations.destroy', $result->id) }}" class="form-horizontal" method="post" style="display: inline-block">
                                {!! csrf_field() !!}
                                <input type="hidden" name="_method" value="DELETE">
                                <button type="submit" class="btn btn-secondary">
                                    <span>
                                        <i class="fa fa-ban" aria-hidden="true"></i>
                                    </span>
                                </button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <footer>
                {{ $results->render('vendor.pagination.bootstrap-4') }}
            </footer>
        </div><!-- ./col-12 -->
    </div><!-- ./row -->
</div><!-- ./container -->
@endsection
