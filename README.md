# mgov-teste

Esse é um teste que eu realizei para a empresa Mgov.

**O objetivo é** atender as necessidades que foram pedidas por e-mail pelo Gestor.

Fique a vontade para criar uma issue ou dar pull request, caso tenha algum problemasugestão.

-------------------

## Instalação:

### Requisitos:
* PHP 7.* ou 7.1.*, mas não o 7.2 (um dos pacotes está dando erro nesta versão).
* Composer
* Node e NPM
* Algum banco de dados, desenvolvi utilizando o MySQL.
* Git
* Docker + docker compose (não é obrigatório, mas vai lhe ajudar e muito caso tenha).

Obtenha a aplicação:
```terminal
git clone https://caueprado@bitbucket.org/caueprado/mgov-teste.git
```
Agora, vamos configurar as variáveis de ambiente:

```terminal
cp .env.example .env
```
Imaginando que você tenha o docker-compose e vai querer trabalhar com meu docker-compose.yml, você precisa então configurar as seguintes variáveis:

```terminal
DB_CONNECTION=mysql
DB_HOST=mgov-mysql
DB_PORT=3306
DB_DATABASE=mgov
DB_USERNAME=mgov
DB_PASSWORD=secret
```
```terminal
QUEUE_DRIVER=database
```

E agora é muito importante que você tenha um serviço de e-mail, para desenvolvimento, eu gosto muito do mailtrap, que pode ser encontrado no seguinte link:
[Mailtrap](https://mailtrap.io/)

Logo, suas variáveis ficaram parecidas com as seguintes informações:
```terminal
MAIL_DRIVER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=PEGA_NO_MAILTRAP
MAIL_PASSWORD=PEGA_NO_MAILTRAP
MAIL_ENCRYPTION=null
```
Agora, estão duas variáveis muito importantes dentro do projeto são elas:
```terminal
MAIL_PLAN_IMPORTED=email_da_pessoa@teste.com
MAIL_PLAN_NAME=Nome_Pessoa_Que_Vai_Receber_O_Email
MAIL_NAME=Caue
```
Temos ainda um plus, que já vai estar no seu, a seguinte variável:
```terminal
FAKER_LANGUAGE=pt_BR
```
Essa variável indica qual será o idioma do pacote faker, isso, se você quiser popular suas tabelas com dados faker.

Agora, vamos instalar realmente o Laravel, utilizando o seguinte comando:
```terminal
composer install
```
Em ambientes de produção, o comando é o mesmo, porém, deve-se adicionar a flag `--no-dev`.

___
#### ATENÇÃO, quem estiver trabalhando com Docker:

Querido, se você está trabalhando com o docker, precisa ficar atento, pois alguns comandos do artisan, você precisa estar executando a partir do seu container. Exemplo:

```terminal
docker exec -it mgov-nginx bash
```
O comando acima vai me mandar para dentro do meu container, para poder rodar instruções dentro do bash, assim, eu posso executar instruções como criar o banco de dados, sem problemas, exemplo:

```terminal
php artisan migrate --seed
```

Para sair do container basta apertar a seguinte combinação no teclado: `Ctrl + D` , ou digitar exit e dar um `Enter`.
___


Agora, vamos ober uma key para nossa aplicação:
```terminal
php artisan key:generate
```

Vamos instalar os pacotes do front-end:
```terminal
npm install
```
Novamente, se estiver em ambiente de produção, deve-se adicionar a seguinte flag `--production`.

Vamos instalar as tabelas no banco de dados, para isso, execute:
```terminal
php artisan migrate:fresh
```

Se, suas variáveis que estão no `dotEnv`, estiverem setadas para produção, será perguntado se realmente deseja rodar o comando anterior, aí, basta digitar yes e dar um enter.

Em ambientes Linux, precisamos dar permissões especiais para algumas pastas, para evitar erros na aplicação, por isso, execute o seguinte comando:
```terminal
sudo chgrp -R www-data storage bootstrap/cache
```
E agora, por último, mas não menos importante, vamos executar as filas no servidor, para isso, execute o seguinte comando:

```terminal
nohup php artisan queue:work --daemon &
```

Pronto, se divirta. Qualquer problema, entrar em contato.
