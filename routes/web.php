<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});
Route::group(['middleware' => 'auth'], function(){
    Route::resource('mgov-ceps', 'MgovCepsController');

    Route::resource('mgov-locations', 'MgovLocationsController');

    Route::post('import-excel-mgov-template', 'ImportExcelMgovTemplateController@store')->name('import-excel-mgov-template.store');
    Route::post('create-lat-lng-from-mgov-ceps', 'GeolocationController@getLatitudeLongitude')->name('create-lat-lng-from-mgov-ceps.location');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
