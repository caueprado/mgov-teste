<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('mgov-ceps', 'Api\MgovCepsController', [
    'except' => ['create', 'edit']
]);

Route::resource('mgov-locations', 'Api\MgovLocationsController', [
    'except' => ['create', 'edit']
]);

Route::post('import-excel-mgov-template', 'Api\ImportExcelMgovTemplateController@store');
Route::post('create-lat-lng-from-mgov-ceps', 'Api\GeolocationController@getLatitudeLongitude');
