let mix = require('laravel-mix');

mix.styles([
	'node_modules/font-awesome/css/font-awesome.min.css',
	'node_modules/bootstrap/dist/css/bootstrap.min.css'
], 'public/css/app.css').sourceMaps();

mix.copy([
	'node_modules/font-awesome/fonts/',
], 'public/fonts/', false);


mix.scripts([
	'node_modules/jquery/dist/jquery.min.js',
	'node_modules/popper.js/dist/umd/popper.min.js',
	'node_modules/bootstrap/dist/js/bootstrap.min.js'
], 'public/js/app.js').sourceMaps();

mix.browserSync({
	proxy: 'localhost:8000',
	notify: false
});
