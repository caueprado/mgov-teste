<?php

namespace Mgov\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class MgovCepValidator extends LaravelValidator
{
    use ValidatorDataFunctions;

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'mgov_id' => [
                'required',
                'max:255',
                'unique:mgov_ceps,mgov_id'
            ],
            'cep' => [
                'required',
                'max:8'
            ],
            'pais' => [
                'nullable',
                'max:255'
            ],
        ],
        ValidatorInterface::RULE_UPDATE => [
            'mgov_id' => [
                'sometimes',
                'required',
                'max:255',
                'unique:mgov_ceps,mgov_id,id'
            ],
            'cep' => [
                'sometimes',
                'required',
                'max:8'
            ],
            'pais' => [
                'nullable',
                'max:255'
            ],
        ],
        'updateOrCreate' => [
            'mgov_id' => [
                'required',
                'max:255',
            ],
            'cep' => [
                'required',
                'max:8'
            ],
            'pais' => [
                'nullable',
                'max:255'
            ],
        ],
    ];

    public function getRules($action = null)
    {
        if (array_key_exists('cep', $this->data)) {
            $this->data['cep'] = $this->returnOnlyNumbers($this->data['cep']);
        }

        return parent::getRules($action);
    }
}
