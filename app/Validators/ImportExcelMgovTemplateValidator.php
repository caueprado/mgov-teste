<?php

namespace Mgov\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class ImportExcelMgovTemplateValidator extends LaravelValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'file' => [
                'required',
                'file',
                'mimes:xls,xlsx',
            ],
        ],
        ValidatorInterface::RULE_UPDATE => [
            'file' => [
                'required',
                'file',
                'mimes:xls,xlsx',
            ],
        ],
    ];
}
