<?php

namespace Mgov\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class MgovLocationValidator extends LaravelValidator
{

    protected $rules = [
        'preRule' => [
            'mgov_ceps_id' => [
                'required',
                'integer',
                'exists:mgov_ceps,id',
            ],
        ],
        ValidatorInterface::RULE_CREATE => [
            'latitude' => [
                'required',
                'max:255',
            ],
            'longitude' => [
                'required',
                'max:255',
            ],
            'mgov_ceps_id' => [
                'required',
                'integer',
                'unique:mgov_locations,mgov_ceps_id',
                'exists:mgov_ceps,id',
            ],
        ],
        ValidatorInterface::RULE_UPDATE => [
            'latitude' => [
                'sometimes',
                'max:255',
            ],
            'longitude' => [
                'sometimes',
                'max:255',
            ],
            'mgov_ceps_id' => [
                'sometimes',
                'integer',
                'exists:mgov_ceps,id',
                'unique:mgov_locations,mgov_ceps_id',
            ],
        ],
        'updateOrCreate' => [
            'latitude' => [
                'required',
                'max:255',
            ],
            'longitude' => [
                'required',
                'max:255',
            ],
            'mgov_ceps_id' => [
                'required',
                'integer',
                'exists:mgov_ceps,id',
            ],
        ],
    ];
}
