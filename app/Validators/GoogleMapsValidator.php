<?php

namespace Mgov\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class GoogleMapsValidator extends LaravelValidator
{
    use ValidatorDataFunctions;

    protected $rules = [
        'cep' => [
            'required',
            'max:8'
        ],
        'pais' => [
            'nullable',
            'max:255'
        ],
    ];

    public function getRules($action = null)
    {
        if (array_key_exists('cep', $this->data)) {
            $this->data['cep'] = $this->returnOnlyNumbers($this->data['cep']);
        }

        return parent::getRules($action);
    }
}
