<?php

namespace Mgov\Validators;

trait ValidatorDataFunctions
{
    public function returnOnlyNumbers($value)
    {
        return $value? preg_replace('/[^0-9]/', '', $value): null;
    }
}
