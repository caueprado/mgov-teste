<?php

namespace Mgov\Http\Controllers;

use Illuminate\Http\Request;

use Mgov\Services\MgovCepService;
use Mgov\Repositories\Contracts\MgovCepRepository;

class MgovCepsController extends Controller
{
    use ControllerTrait {
        index as traitIndex;
        destroy as traitDestroy;
        store as traitStore;
        show as traitShow;
        update as traitUpdate;
    }

    public $service;
    public $repository;

    public function __construct(MgovCepService $service, MgovCepRepository $repository)
    {
        $this->service = $service;
        $this->repository = $repository;
    }

    public function index()
    {
        $results = $this->traitIndex();
        return view('mgov_ceps.index', compact('results'));
    }

    public function show($id)
    {
        $result = $this->traitShow($id);
        return view('mgov_ceps.show', compact('result'));
    }

    public function create()
    {
        return view('mgov_ceps.create');
    }

    public function store(Request $request)
    {
        $result = $this->traitStore($request);
        return view('mgov_ceps.show', compact('result'));
    }

    public function edit($id)
    {
        $result = $this->traitShow($id);
        return view('mgov_ceps.edit', compact('result'));
    }

    public function update(Request $request, $id)
    {
        $result = $this->traitUpdate($request, $id);
        return view('mgov_ceps.show', compact('result'));
    }

    public function destroy($id)
    {
        $results = $this->traitDestroy($id);
        return response()->redirectToRoute('mgov-ceps.index');
    }
}
