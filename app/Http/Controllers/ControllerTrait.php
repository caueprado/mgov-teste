<?php

namespace Mgov\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Prettus\Validator\Exceptions\ValidatorException;
use \Exception;

trait ControllerTrait
{
    public function index()
    {
        return $this->repository->paginate();
    }

    public function store(Request $request)
    {
        try {
            return $this->service->create($request->all());
        } catch (ValidatorException $e) {
            return $e;
        }
    }

    public function show($id)
    {
        try {
            return $this->repository->find($id);
        } catch (ModelNotFoundException $e) {
            return 'erro';
        }
    }

    public function update(Request $request, $id)
    {
        try {
            return $this->service->update($request->all(), $id);
        } catch (ModelNotFoundException $e) {
            return 'erro';
        } catch (ValidatorException $e) {
            return $e;
        } catch (Exception $e) {
            return 'erro';
        }
    }

    public function destroy($id)
    {
        try {
            return $this->repository->delete($id);
        } catch (QueryException $e) {
            return 'erro';
        } catch (ModelNotFoundException $e) {
            return 'erro';
        } catch (Exception $e) {
            return 'erro';
        }
    }
}
