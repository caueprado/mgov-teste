<?php

namespace Mgov\Http\Controllers;

use Illuminate\Http\Request;

use Mgov\Services\ImportExcelMgovTemplateService;
use Prettus\Validator\Exceptions\ValidatorException;

class ImportExcelMgovTemplateController extends Controller
{
    public $service;

    public function __construct(ImportExcelMgovTemplateService $service)
    {
        $this->service = $service;
    }

    public function store(Request $request)
    {
        try {
            $result = $this->service->create($request->all());
            return response()->redirectToRoute('mgov-ceps.index');
        } catch (ValidatorException $e) {
            return $e;
        } catch (\Exception $e) {
            return $e;
        }
    }
}
