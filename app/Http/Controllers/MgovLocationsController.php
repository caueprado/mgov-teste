<?php

namespace Mgov\Http\Controllers;

use Illuminate\Http\Request;

use Mgov\Services\MgovLocationService;
use Mgov\Repositories\Contracts\MgovLocationRepository;

class MgovLocationsController extends Controller
{
    use ControllerTrait {
        index as traitIndex;
        destroy as traitDestroy;
        store as traitStore;
        show as traitShow;
        update as traitUpdate;
    }

    public $service;
    public $repository;

    public function __construct(MgovLocationService $service, MgovLocationRepository $repository)
    {
        $this->service = $service;
        $this->repository = $repository;
    }

    public function index()
    {
        $results = $this->traitIndex();
        return view('mgov_locations.index', compact('results'));
    }

    public function create()
    {
        return view('mgov_locations.create');
    }

    public function edit($id)
    {
        $result = $this->traitShow($id);
        return view('mgov_locations.edit', compact('result'));
    }

    public function show($id)
    {
        $result = $this->traitShow($id);
        return view('mgov_locations.show', compact('result'));
    }

    public function store(Request $request)
    {
        $result = $this->traitStore($request);
        return view('mgov_locations.show', compact('result'));
    }

    public function update(Request $request, $id)
    {
        $result = $this->traitUpdate($request, $id);
        return view('mgov_locations.show', compact('result'));
    }

    public function destroy($id)
    {
        $results = $this->traitDestroy($id);
        return response()->redirectToRoute('mgov-locations.index');
    }
}
