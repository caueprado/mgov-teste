<?php

namespace Mgov\Http\Controllers;

use Illuminate\Http\Request;

use Mgov\Services\MgovLocationService;
use Mgov\Repositories\Contracts\MgovCepRepository;

use Mgov\Events\CreateLocationFromMgovCepModel;

class GeolocationController extends Controller
{
    protected $service;
    protected $repository;

    public function __construct(MgovLocationService $service, MgovCepRepository $repository)
    {
        $this->service = $service;
        $this->repository = $repository;
    }

    public function getLatitudeLongitude(Request $request)
    {
        event(new CreateLocationFromMgovCepModel());
        return response()->redirectToRoute('mgov-locations.index');
    }
}
