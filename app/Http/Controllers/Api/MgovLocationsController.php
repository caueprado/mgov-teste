<?php

namespace Mgov\Http\Controllers\Api;

use Illuminate\Http\Request;
use Mgov\Http\Controllers\Controller;
use Mgov\Http\Controllers\ControllerTrait;

use Mgov\Services\MgovLocationService;
use Mgov\Repositories\Contracts\MgovLocationRepository;

class MgovLocationsController extends Controller
{
    use ControllerTrait;

    public $service;
    public $repository;

    public function __construct(MgovLocationService $service, MgovLocationRepository $repository)
    {
        $this->service = $service;
        $this->repository = $repository;
    }
}
