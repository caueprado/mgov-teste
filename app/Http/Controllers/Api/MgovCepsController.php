<?php

namespace Mgov\Http\Controllers\Api;

use Illuminate\Http\Request;
use Mgov\Http\Controllers\Controller;
use Mgov\Http\Controllers\ControllerTrait;

use Mgov\Services\MgovCepService;
use Mgov\Repositories\Contracts\MgovCepRepository;

class MgovCepsController extends Controller
{
    use ControllerTrait;

    public $service;
    public $repository;

    public function __construct(MgovCepService $service, MgovCepRepository $repository)
    {
        $this->service = $service;
        $this->repository = $repository;
    }
}
