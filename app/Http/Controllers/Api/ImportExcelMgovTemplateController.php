<?php

namespace Mgov\Http\Controllers\Api;

use Illuminate\Http\Request;
use Mgov\Http\Controllers\Controller;

use Mgov\Services\ImportExcelMgovTemplateService;
use Prettus\Validator\Exceptions\ValidatorException;

class ImportExcelMgovTemplateController extends Controller
{
    public $service;

    public function __construct(ImportExcelMgovTemplateService $service)
    {
        $this->service = $service;
    }

    public function store(Request $request)
    {
        try {
            return $this->service->create($request->all());
        } catch (ValidatorException $e) {
            return $e;
        } catch (\Exception $e) {
            return $e;
        }
    }
}
