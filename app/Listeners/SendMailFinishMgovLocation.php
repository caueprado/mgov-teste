<?php

namespace Mgov\Listeners;

use Mgov\Events\SendMailAfterFinishCreateOrUpdateMgovLocation;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailFinishMgovLocation implements ShouldQueue
{
    use InteractsWithQueue;

    public function __construct()
    {
        //
    }

    public function handle(SendMailAfterFinishCreateOrUpdateMgovLocation $event)
    {
        \Log::info("Listener SendMailFinishMgovLocation started");

        \Mail::to(env('MAIL_PLAN_IMPORTED'))->queue(new \Mgov\Mail\MgovLocationUpdatedFromMgovCep($event->errors));
        \Log::info("Mail was sent");
        \Log::info("Listener SendMailFinishMgovLocation finished");
    }
}
