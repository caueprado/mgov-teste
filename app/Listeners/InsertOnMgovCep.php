<?php

namespace Mgov\Listeners;

use Mgov\Events\MgovCepCreateFromFile;
use Mgov\Events\InsertOnMgovCepCompleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Mgov\Services\ImportExcelMgovTemplateService;

class InsertOnMgovCep implements ShouldQueue
{
    use InteractsWithQueue;

    protected $service;

    public function __construct(ImportExcelMgovTemplateService $service)
    {
        $this->service = $service;
    }

    public function handle(MgovCepCreateFromFile $event)
    {
        \Log::info("Listener InsertOnMgovCep started");

        $result = $this->service->createWithEvent($event->file);
        event(new InsertOnMgovCepCompleted($result));

        \Log::info("Listener InsertOnMgovCep finished");
    }
}
