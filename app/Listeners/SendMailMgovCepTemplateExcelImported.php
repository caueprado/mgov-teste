<?php

namespace Mgov\Listeners;

use Mgov\Events\MgovCepTemplateExcelCompleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailMgovCepTemplateExcelImported implements ShouldQueue
{
    use InteractsWithQueue;
    public function __construct()
    {
        //
    }

    public function handle(MgovCepTemplateExcelCompleted $event)
    {
        \Log::info("Listener SendMailMgovCepTemplateExcelImported started");

        $data = $event->data;

        \Mail::to(env('MAIL_PLAN_IMPORTED'))->queue(new \Mgov\Mail\MgovCepsImported($data));
        \Log::info("Mail was sent");
        \Log::info("Listener SendMailMgovCepTemplateExcelImported finished");
    }
}
