<?php

namespace Mgov\Listeners;

use Mgov\Events\InsertOnMgovCepCompleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Mgov\MgovCepExcel\CareResultMgovCepExcelImported;

use Mgov\Events\MgovCepTemplateExcelCompleted;

class CareResultMgovCepExcel implements ShouldQueue
{
    use InteractsWithQueue;

    protected $clearResult;

    public function __construct(CareResultMgovCepExcelImported $clearResult)
    {
        $this->clearResult = $clearResult;
    }

    public function handle(InsertOnMgovCepCompleted $event)
    {
        \Log::info("Listener CareResultMgovCepExcel started");
        $result = $event->results;
        $result = $this->clearResult->care($result);

        event(new MgovCepTemplateExcelCompleted($result));

        \Log::info("Listener CareResultMgovCepExcel finished");
    }
}
