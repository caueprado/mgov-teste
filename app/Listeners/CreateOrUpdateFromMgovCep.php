<?php

namespace Mgov\Listeners;

use Mgov\Events\CreateLocationFromMgovCepModel;
use Mgov\Events\SendMailAfterFinishCreateOrUpdateMgovLocation;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Prettus\Validator\Exceptions\ValidatorException;
use Mgov\Services\MgovLocationService;
use Mgov\Repositories\Contracts\MgovCepRepository;

class CreateOrUpdateFromMgovCep implements ShouldQueue
{
    use InteractsWithQueue;

    public $service;
    public $repository;

    public function __construct(MgovLocationService $service, MgovCepRepository $repository)
    {
        $this->service = $service;
        $this->repository = $repository;
    }

    public function handle(CreateLocationFromMgovCepModel $event)
    {
        \Log::info("Listener CreateOrUpdateFromMgovCep started");

        $failedIds = collect([]);

        $mgovCepsIds = $this->repository->all(['id']);

        $mgovCepsIds->each(function ($item, $key) use ($failedIds) {
            try {
                $data = [
                    'mgov_ceps_id' => $item->id
                ];
                \Log::info("Listener CreateOrUpdateFromMgovCep id> " . $item->id);

                if (!$this->service->createOrUpdate($data)) {
                    $failedIds->push(['item' => $item->id, 'reason' => 'Ocorreu um erro na aplicação.']);
                }
            } catch (ValidatorException $e) {
                $failedIds->push(['item' => $item->id, 'reason' => 'Falha na validação dos dados']);
            } catch (\Exception $e) {
                $failedIds->push(['item' => $item->id, 'reason' => 'Ocorreu uma exceção genérica']);
            }
        });
        event(new SendMailAfterFinishCreateOrUpdateMgovLocation($failedIds));
        \Log::info("Listener CreateOrUpdateFromMgovCep finished");
    }
}
