<?php

namespace Mgov\Models;

use Illuminate\Database\Eloquent\Model;
use Mgov\Validators\ValidatorDataFunctions;

class MgovCep extends Model
{
    use ValidatorDataFunctions;

    protected $table = 'mgov_ceps';
    protected $fillable = [
        'mgov_id',
        'cep',
        'pais',
    ];

    public function location()
    {
        return $this->hasMany('Mgov\Models\MgovLocation', 'mgov_ceps_id', 'id');
    }

    public function setCepAttribute($value)
    {
        $this->attributes['cep'] = $this->returnOnlyNumbers($value);
    }

    public function formatCepAttribute($value)
    {
        if (strlen($value) ===  8) {
            return preg_replace('/(\d{5})(\d{3})/', '$1-$2', $value);
        }
        return $value;
    }
}
