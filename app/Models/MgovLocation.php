<?php

namespace Mgov\Models;

use Illuminate\Database\Eloquent\Model;

class MgovLocation extends Model
{
    protected $table = 'mgov_locations';
    protected $fillable = [
        'latitude',
        'longitude',
        'mgov_ceps_id'
    ];

    public function mgovCep()
    {
        return $this->belongsTo('Mgov\Models\MgovCep', 'mgov_ceps_id', 'id');
    }
}
