<?php

namespace Mgov\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MgovCepRepository
 * @package namespace App\Repositories\Contracts;
 */
interface MgovCepRepository extends RepositoryInterface
{
    //
}
