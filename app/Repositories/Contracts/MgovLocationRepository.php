<?php

namespace Mgov\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MgovLocationRepository
 * @package namespace Mgov\Repositories\Contracts;
 */
interface MgovLocationRepository extends RepositoryInterface
{
    //
}
