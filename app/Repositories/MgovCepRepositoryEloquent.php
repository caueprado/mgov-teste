<?php

namespace Mgov\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Mgov\Repositories\Contracts\MgovCepRepository;
use Mgov\Models\MgovCep;
use Mgov\Validators\MgovCepValidator;

class MgovCepRepositoryEloquent extends BaseRepository implements MgovCepRepository
{

    protected $fieldSearchable = [
        'mgov_id',
        'cep'
    ];

    public function model()
    {
        return MgovCep::class;
    }

    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
