<?php

namespace Mgov\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Mgov\Repositories\Contracts\MgovLocationRepository;
use Mgov\Models\MgovLocation;
use Mgov\Validators\MgovLocationValidator;

/**
 * Class MgovLocationRepositoryEloquent
 * @package namespace Mgov\Repositories;
 */
class MgovLocationRepositoryEloquent extends BaseRepository implements MgovLocationRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MgovLocation::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
