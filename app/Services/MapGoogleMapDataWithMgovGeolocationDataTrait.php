<?php

namespace Mgov\Services;

use Mgov\MgovCepExcel\MgovGeolocation;

trait MapGoogleMapDataWithMgovGeolocationDataTrait
{
    private $map = [
        'address_components' => [
            'route' => [
                'long_name' => 'setStreet',
            ],
            'postal_code' => [
                'long_name' => 'setZipCode'
            ],
            'sublocality_level_1' => [
                'long_name' => 'setNeighborhood'
            ],
            'administrative_area_level_2' => [
                'long_name' => 'setCity',
            ],
            'administrative_area_level_1' =>[
                'long_name' => 'setState',
                'short_name' => 'setStateAbbr',
            ],
            'country' => [
                'long_name' => 'setCountry',
                'short_name' => 'setCountryAbbr'
            ],
        ],
        'geometry' => [
            'location' => [
                'lat' => 'setLatitude',
                'lng' => 'setLongitude',
            ],
        ],
    ];

    public function getMap()
    {
        return $this->map;
    }

    private function map($resultFromGoogleMaps)
    {
        $addressComponents = $resultFromGoogleMaps->address_components;
        $addressComponents = collect($addressComponents);

        $this->getAddressComponentsFromGoogleMaps($addressComponents);

        $geometry = $resultFromGoogleMaps->geometry;

        $this->getGeometryFromGoogleMaps($geometry);
    }

    private function getAddressComponentsFromGoogleMaps($addressComponents)
    {
        $addressComponents->each(function ($item, $key) {
            $types = collect($item->types);
            $types->each(function ($itemType, $keyType) use ($item, $key) {
                foreach ($this->map['address_components'] as $keyMap => $valueMap) {
                    if ($itemType == $keyMap) {
                        foreach ($valueMap as $keyMap2 => $valueMap2) {
                            $this->mgovGeolocation->$valueMap2($item->$keyMap2);
                        }
                        return false;
                    }
                }
            });
        });
    }

    private function getGeometryFromGoogleMaps($geometry)
    {
        foreach ($this->map['geometry']['location'] as $key => $value) {
            $this->mgovGeolocation->$value($geometry->location->$key);
        }
    }

    private function getMgovGeolocaiton():MgovGeolocation
    {
        return $this->mgovGeolocation;
    }
}
