<?php

namespace Mgov\Services;

use Mgov\Validators\ImportExcelMgovTemplateValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

use Mgov\MgovCepExcel\ImportExcelMgovTemplateTrait;
use Mgov\MgovCepExcel\ImportExcelService;

use Mgov\Events\MgovCepCreateFromFile;

class ImportExcelMgovTemplateService
{
    use ImportExcelMgovTemplateTrait;

    protected $templateExcelValidator;
    protected $importExcelService;
    protected $mgovCepService;

    public function __construct(ImportExcelMgovTemplateValidator $templateExcelValidator, ImportExcelService $importExcelService, MgovCepService $mgovCepService)
    {
        $this->templateExcelValidator = $templateExcelValidator;
        $this->importExcelService = $importExcelService;
        $this->mgovCepService = $mgovCepService;
    }

    public function create(array $data)
    {
        $this->templateExcelValidator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);
        $path = $this->saveAndGetPath($data['file']);

        event(new MgovCepCreateFromFile($path));
        return ['success' => true, 'message' => 'The Import Excel Event has started.'];
    }
}
