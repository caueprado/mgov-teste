<?php

namespace Mgov\Services;

use Mgov\MgovCepExcel\MgovGeolocation;

class GoogleMapsService
{
    use MapGoogleMapDataWithMgovGeolocationDataTrait;

    protected $mgovGeolocation;

    public function __construct(MgovGeolocation $mgovGeolocation)
    {
        $this->mgovGeolocation = $mgovGeolocation;
    }

    public function getGeolocationByAddress(array $data)
    {
        $address = $this->getAddressFormated($data);
        $result = json_decode($this->getLocation($address));
        $result = isset($result)? $result->results[0]:null;
        if ($result) {
            $this->map($result);
        }
        return $this->mgovGeolocation;
    }

    private function getAddressFormated($data)
    {
        return $data['cep'] . (isset($data['pais'])? ", " . ucfirst($data['pais']) :null);
    }

    private function getLocation(string $address)
    {
        return \GoogleMaps::load('geocoding')->setParam([
            'address' => $address,
        ])->get();
    }
}
