<?php

namespace Mgov\Services;

use Mgov\Repositories\Contracts\MgovCepRepository;
use Mgov\Validators\MgovCepValidator;

class MgovCepService extends Service
{
    public function __construct(MgovCepRepository $repository, MgovCepValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }
}
