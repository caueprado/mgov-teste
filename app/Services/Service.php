<?php

namespace Mgov\Services;

use Prettus\Validator\Exceptions\ValidatorException;
use Prettus\Validator\Contracts\ValidatorInterface;

abstract class Service
{
    protected $repository;
    protected $validator;

    public function find($id, $columns = ['*'])
    {
        return $this->repository->find($id, $columns);
    }

    public function create(array $data)
    {
        $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);
        return $this->repository->skipPresenter()->create($data);
    }

    public function update(array $data, $id)
    {
        $this->validator->with($data)->setId($id)->passesOrFail(ValidatorInterface::RULE_UPDATE);
        return $this->repository->skipPresenter()->update($data, $id);
    }

    public function updateOrCreate(array $ruleToUpdateOrCreate, array $data)
    {
        $this->validator->with($data)->passesOrFail('updateOrCreate');
        return $this->repository->skipPresenter()->updateOrCreate($ruleToUpdateOrCreate, $data);
    }
}
