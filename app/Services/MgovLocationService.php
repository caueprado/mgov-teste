<?php

namespace Mgov\Services;

use Mgov\Repositories\Contracts\MgovLocationRepository;
use Mgov\Validators\MgovLocationValidator;

use Mgov\Validators\GoogleMapsValidator;
use Carbon\Carbon;

class MgovLocationService extends Service
{
    protected $googleMapsValidator;
    protected $googleMapsService;

    protected $mgovCepService;

    public function __construct(MgovLocationRepository $repository, MgovLocationValidator $validator, GoogleMapsValidator $googleMapsValidator, GoogleMapsService $googleMapsService, MgovCepService $mgovCepService)
    {
        $this->repository = $repository;
        $this->validator = $validator;

        $this->googleMapsValidator = $googleMapsValidator;
        $this->googleMapsService = $googleMapsService;

        $this->mgovCepService = $mgovCepService;
    }

    private function preChanges(array $data)
    {
        $this->validator->with($data)->passesOrFail('preRule');
        return $this->mgovCepService->find($data['mgov_ceps_id'], ['cep', 'pais'])->toArray();
    }

    public function create(array $data)
    {
        $mgovCep = $this->preChanges($data);

        $this->googleMapsValidator->with($mgovCep)->passesOrFail();
        $mgovGeolocation = $this->googleMapsService->getGeolocationByAddress($mgovCep);

        $newData = $this->getNewData($data['mgov_ceps_id'], $mgovGeolocation);

        return parent::create($newData);
    }

    public function update(array $data, $id)
    {
        $mgovCep = $this->preChanges($data);

        $this->googleMapsValidator->with($mgovCep)->passesOrFail();
        $mgovGeolocation = $this->googleMapsService->getGeolocationByAddress($mgovCep);

        $newData = $this->getNewData($data['mgov_ceps_id'], $mgovGeolocation);

        return parent::update($newData, $id);
    }

    public function createOrUpdate(array $data)
    {
        $mgovCep = $this->preChanges($data);

        $find = $this->repository->findByField('mgov_ceps_id', $data['mgov_ceps_id'], ['id','updated_at']);

        if ($find->count() > 0 && $find->first()->updated_at > Carbon::now()->addDays(-10)) {
            return $find;
        }

        $this->googleMapsValidator->with($mgovCep)->passesOrFail();
        $mgovGeolocation = $this->googleMapsService->getGeolocationByAddress($mgovCep);

        $newData = $this->getNewData($data['mgov_ceps_id'], $mgovGeolocation);
        return parent::updateOrCreate($this->getRulesToUpdateOrCreate($newData), $newData);
    }

    private function getRulesToUpdateOrCreate(array $data):array
    {
        return [
            'mgov_ceps_id' => $data['mgov_ceps_id']
        ];
    }

    private function getNewData($mgovCepsId, $mgovGeolocation)
    {
        return [
            'latitude' => $mgovGeolocation->getLatitude(),
            'longitude' => $mgovGeolocation->getLongitude(),
            'mgov_ceps_id' => $mgovCepsId,
        ];
    }
}
