<?php

namespace Mgov\MgovCepExcel;

class MgovGeolocation
{
    protected $zipCode;
    protected $street;
    protected $postCode;
    protected $neighborhood;
    protected $city;
    protected $state;
    protected $stateAbbr;
    protected $country;
    protected $countryAbbr;
    protected $latitude;
    protected $longitude;

    /**
    * Get the value of address
    */
    public function getAddress()
    {
        return $this->street . ', ' . $this->zipCode;
    }

    /**
    * Get the value of zipCode
    */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
    * Set the value of zipCode
    *
    * @return  self
    */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
    * Get the value of street
    */
    public function getStreet()
    {
        return $this->street;
    }

    /**
    * Set the value of street
    *
    * @return  self
    */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
    * Get the value of postCode
    */
    public function getPostCode()
    {
        return $this->postCode;
    }

    /**
    * Set the value of postCode
    *
    * @return  self
    */
    public function setPostCode($postCode)
    {
        $this->postCode = $postCode;

        return $this;
    }

    /**
    * Get the value of neighborhood
    */
    public function getNeighborhood()
    {
        return $this->neighborhood;
    }

    /**
    * Set the value of neighborhood
    *
    * @return  self
    */
    public function setNeighborhood($neighborhood)
    {
        $this->neighborhood = $neighborhood;

        return $this;
    }

    /**
    * Get the value of city
    */
    public function getCity()
    {
        return $this->city;
    }

    /**
    * Set the value of city
    *
    * @return  self
    */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
    * Get the value of state
    */
    public function getState()
    {
        return $this->state;
    }

    /**
    * Set the value of state
    *
    * @return  self
    */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
    * Get the value of stateAbbr
    */
    public function getStateAbbr()
    {
        return $this->stateAbbr;
    }

    /**
    * Set the value of stateAbbr
    *
    * @return  self
    */
    public function setStateAbbr($stateAbbr)
    {
        $this->stateAbbr = $stateAbbr;

        return $this;
    }

    /**
    * Get the value of country
    */
    public function getCountry()
    {
        return $this->country;
    }

    /**
    * Set the value of country
    *
    * @return  self
    */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
    * Get the value of country
    */
    public function getCountryAbbr()
    {
        return $this->countryAbbr;
    }

    /**
    * Set the value of country
    *
    * @return  self
    */
    public function setCountryAbbr($countryAbbr)
    {
        $this->countryAbbr = $countryAbbr;

        return $this;
    }

    /**
    * Get the value of latitude
    */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
    * Set the value of latitude
    *
    * @return  self
    */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
    * Get the value of longitude
    */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
    * Set the value of longitude
    *
    * @return  self
    */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }
}
