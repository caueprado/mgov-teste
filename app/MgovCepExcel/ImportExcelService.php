<?php

namespace Mgov\MgovCepExcel;

use Illuminate\Http\UploadedFile;
use Maatwebsite\Excel\Facades\Excel;

use Maatwebsite\Excel\Collections\CellCollection;
use Maatwebsite\Excel\Collections\RowCollection;

class ImportExcelService
{
    protected $data;

    public function __construct()
    {
        $this->data = collect([]);
    }

    public function extractData(string $file, $firstRowIsHeader = true)
    {
        try {
            $excelHandler = Excel::load($file, function ($reader) use ($firstRowIsHeader) {
                $firstRowIsHeader?: $reader->noHeading();
                // Loop through all sheets
                $reader->each(function ($cellCollection) {
                    $this->data->push($cellCollection->all());
                });
            });
            return $this->data;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
