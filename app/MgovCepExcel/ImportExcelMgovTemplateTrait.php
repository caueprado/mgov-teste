<?php

namespace Mgov\MgovCepExcel;

use Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\UploadedFile;

trait ImportExcelMgovTemplateTrait
{
    public function createWithEvent(string $file)
    {
        if (!$this->fileExists($file)) {
            throw new \Exception("Arquivo não encontrado");
        }

        $dataFromFile = $this->importExcelService->extractData($file);
        $result = collect([]);

        $dataFromFile->each(function ($item, $key) use ($result) {
            if ($item['id'] == null || $item['cep'] == null) {
                return false;
            }

            try {
                $rules = $this->getRuleToUpdateOrCreate($item);
                $data = $this->prepareItem($item);

                if ($this->mgovCepService->updateOrCreate($rules, $data)) {
                    $result->push($this->formatResultFromEvent(true, $key + 1));
                }
            } catch (ValidatorException $e) {
                $result->push($this->formatResultFromEvent(false, $key + 1, 'Erro na validação'));
            } catch (\Exception $e) {
                $msg = $e->getMessage();
                $result->push($this->formatResultFromEvent(false, $key + 1), $msg);
            }
        });

        return $result;
    }

    public function saveAndGetPath(UploadedFile $file)
    {
        $pathSaved = $file->storeAs('mgov-excel', $file->getClientOriginalName(), 'local');
        return Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix() . $pathSaved;
    }

    private function fileExists(string $realPath)
    {
        return File::exists($realPath);
    }

    private function getRuleToUpdateOrCreate(array $data, $rule = 'id')
    {
        if (array_key_exists($rule, $data)) {
            return [
                'mgov_id' => (string) $data[$rule]
            ];
        }
        return null;
    }

    private function prepareItem(array $item)
    {
        $data = [
            'mgov_id' => null,
            'cep' => null,
            'pais' => null,
        ];
        $column2 = array_key_exists('cep', $item)? $this->clearInput($item['cep']) :null;

        $data['mgov_id'] = array_key_exists('id', $item)? (string) $item['id']:null;
        $data['cep'] = array_key_exists(0, $column2)? (string)$column2[0]:null;
        $data['pais'] = array_key_exists(1, $column2)? (string)$column2[1]:null;
        return $data;
    }

    private function clearInput(string $input):array
    {
        $array = str_split($input, strpos($input, ","));
        array_key_exists(1, $array)? $array[1] = str_replace(",", "", $array[1]) : $array[1] = "";
        $array[1] = trim($array[1]);
        return strlen($array[1]) > 1? $array: array_pop($array[1]);
    }

    private function formatResultFromEvent(bool $success, int $row, string $reason = null):array
    {
        return [
            'success' => $success,
            'row' => $row,
            'reason' => $reason
        ];
    }
}
