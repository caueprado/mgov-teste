<?php

namespace Mgov\MgovCepExcel;

use Illuminate\Support\Collection;

class CareResultMgovCepExcelImported
{
    protected $positiveResult;
    protected $negativeResult;

    public function __construct()
    {
        $this->positiveResult = collect([]);
        $this->negativeResult = collect([]);
    }

    public function care(Collection $resultFromImportExcel)
    {
        $resultFromImportExcel->each(function ($item, $key) {
            if (array_key_exists('success', $item) && $item['success'] == true) {
                $this->positiveResult->push($item);
            } else {
                $this->negativeResult->push($item);
            }
        });

        return $this->getResult($resultFromImportExcel->count());
    }

    private function getResult(int $totalItens)
    {
        $result = new \StdClass;
        $result->positiveResult = $this->positiveResult;
        $result->negativeResult = $this->negativeResult;
        $result->totalItens = $totalItens;

        return $result;
    }
}
