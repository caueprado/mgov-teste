<?php

namespace Mgov\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'Mgov\Events\MgovCepCreateFromFile' => [
            'Mgov\Listeners\InsertOnMgovCep',
        ],
        'Mgov\Events\InsertOnMgovCepCompleted' => [
            'Mgov\Listeners\CareResultMgovCepExcel',
        ],
        'Mgov\Events\MgovCepTemplateExcelCompleted' => [
            'Mgov\Listeners\SendMailMgovCepTemplateExcelImported'
        ],
        'Mgov\Events\CreateLocationFromMgovCepModel' => [
            'Mgov\Listeners\CreateOrUpdateFromMgovCep'
        ],
        'Mgov\Events\SendMailAfterFinishCreateOrUpdateMgovLocation' => [
            'Mgov\Listeners\SendMailFinishMgovLocation'
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
