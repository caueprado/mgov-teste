<?php

namespace Mgov\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\Mgov\Repositories\Contracts\MgovCepRepository::class, \Mgov\Repositories\MgovCepRepositoryEloquent::class);
        $this->app->bind(\Mgov\Repositories\Contracts\MgovLocationRepository::class, \Mgov\Repositories\MgovLocationRepositoryEloquent::class);
        //:end-bindings:
    }
}
