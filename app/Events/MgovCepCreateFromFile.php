<?php

namespace Mgov\Events;

class MgovCepCreateFromFile
{
    public $file;

    protected $results;

    public function __construct(string $file)
    {
        $this->file = $file;
    }

    public function getResults()
    {
        return $this->results;
    }

    public function setResults($results)
    {
        $this->results = $results;
    }
}
