<?php

namespace Mgov\Events;

class InsertOnMgovCepCompleted
{
    public $results;

    public function __construct($results)
    {
        $this->results = $results;
    }
}
