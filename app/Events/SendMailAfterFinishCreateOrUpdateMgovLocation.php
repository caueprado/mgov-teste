<?php

namespace Mgov\Events;

class SendMailAfterFinishCreateOrUpdateMgovLocation
{
    public $errors;

    public function __construct($errors)
    {
        $this->errors = $errors;
    }
}
