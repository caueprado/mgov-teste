<?php

namespace Mgov\Events;

class MgovCepTemplateExcelCompleted
{
    public $data;
    
    public function __construct($data)
    {
        $this->data = $data;
    }
}
