<?php

namespace Mgov\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MgovCepsImported extends Mailable
{
    use Queueable, SerializesModels;

    public $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function build()
    {
        return $this
        ->from(env('MAIL_USERNAME'), env('MAIL_NAME', 'Mgov'))
        ->subject('Planilha Importada - Mgov Teste')
        ->markdown('emails.mgov-ceps.imported');
    }
}
