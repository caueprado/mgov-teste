<?php

namespace Mgov\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MgovLocationUpdatedFromMgovCep extends Mailable
{
    use Queueable, SerializesModels;

    public $errors;

    public function __construct($errors)
    {
        $this->errors = $errors;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
        ->from(env('MAIL_USERNAME'), env('MAIL_NAME', 'Mgov'))
        ->subject('A atualização da latitude e longitude a partir dos Ceps foi finalizada')
        ->markdown('emails.mgov-location.updated-from-mgov-cep');
    }
}
