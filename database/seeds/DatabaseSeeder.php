<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(MgovCepsTableSeeder::class, 40);
        $this->call(MgovLocationSeeder::class, 10);
        $this->command->info('----------------------------------------'.PHP_EOL. 'MODEL FACTORY FINALIZADO');
    }

    public function call($class, $extra = null)
    {
        $this->resolve($class)->run($extra);
    }
}
