<?php

use Illuminate\Database\Seeder;

class MgovCepsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run($create = 1)
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('mgov_ceps')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $this->command->info('Start seed mgov_ceps table');
        factory(Mgov\Models\MgovCep::class, $create)->create();
    }
}
