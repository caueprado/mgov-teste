<?php

use Illuminate\Database\Seeder;

class MgovLocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run($create = 1)
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('mgov_locations')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $this->command->info('Start seed mgov_locations table');
        factory(Mgov\Models\MgovLocation::class, $create)->create();
    }
}
