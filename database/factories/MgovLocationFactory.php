<?php

use Faker\Generator as Faker;

$factory->define(Mgov\Models\MgovLocation::class, function (Faker $faker) {


    $mgovCep = null;
    $find = true;
    do {
        $mgovCep = Mgov\Models\MgovCep::all()->random(1)->first();
        $mgovCep = isset($mgovCep)? $mgovCep->id:null;

        //Primeiro verifica se existe o id dessa pessoa na propria tabela
        if ((bool)Mgov\Models\MgovLocation::where('mgov_ceps_id', '=', $mgovCep)->get()->first() != null) {
            continue;
        } else {
            $find = false;
        }
    } while ($find);

    return [
        'latitude' => $faker->latitude,
        'longitude' => $faker->longitude,
        'mgov_ceps_id' => $mgovCep
    ];
});
