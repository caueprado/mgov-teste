<?php

use Faker\Generator as Faker;

$factory->define(Mgov\Models\MgovCep::class, function (Faker $faker) {
    return [
        'mgov_id' => $faker->randomNumber(5, true),
        'cep' => $faker->randomNumber(8, true),
        'pais' => 'brazil',
    ];
});
